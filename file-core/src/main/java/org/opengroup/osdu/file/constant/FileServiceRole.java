package org.opengroup.osdu.file.constant;

public class FileServiceRole {

  public static final String VIEWERS = "service.file.viewers";
  public static final String EDITORS = "service.file.editors";

}
