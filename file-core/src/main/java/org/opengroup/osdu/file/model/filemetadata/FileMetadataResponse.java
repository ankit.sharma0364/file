package org.opengroup.osdu.file.model.filemetadata;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class FileMetadataResponse {
    private String id;
}
