package org.opengroup.osdu.file.constant;

public class FileMetadataConstant {

	public static final String FORWARD_SLASH = "/";
	public static final String STAGING_AREA_EXT = "staging-area";
	public static final String PERSISTENT_AREA_EXT = "persistent-area";
	public static final int HTTP_CODE_400 = 400;
	public static final String INVALID_SOURCE_EXCEPTION = "Invalid source file path to copy from ";
	public static final String METADATA_SAVE_STARTED = "Creating metadata in store";
  public static final String FILE = "file";
	public static final String KIND_SEPRATOR = ":";
	public static final String FILE_SOURCE = "FileSource";

}
